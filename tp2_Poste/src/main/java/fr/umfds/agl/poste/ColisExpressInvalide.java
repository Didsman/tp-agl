package fr.umfds.agl.poste;

public class ColisExpressInvalide extends Exception {

	public ColisExpressInvalide(String string) {
		super(string);
	}

}
