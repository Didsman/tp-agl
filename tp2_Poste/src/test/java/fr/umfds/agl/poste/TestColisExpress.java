package fr.umfds.agl.poste;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class TestColisExpress {

	static Lettre lettre1,lettre2;
	static ColisExpress colis1;
	static SacPostal sac1;
	
	private static float tolerancePrix=0.001f;
	private static float toleranceVolume=0.0000001f;
	
	@BeforeAll
	static void setUp() {
		lettre1 = new Lettre("Le pere Noel","famille Kirik, igloo 5, banquise nord","7877", 25, 0.00018f, Recommandation.un, false);
		lettre2 = new Lettre("Le pere Noel","famille Kouk, igloo 2, banquise nord","5854", 18, 0.00018f, Recommandation.deux, true);
		//Création colis express fonctionnel
		try {
			colis1 = new ColisExpress("Le pere Noel","famille Kaya, igloo 10, terres ouest","7877", 24f, 0.02f, Recommandation.deux, "train electrique", 200f,true);
		} catch (ColisExpressInvalide e) {
			assertTrue(e.getMessage().equals("poids incohérent, votre colis ne pourra pas être acheminé."));
		}
		sac1 = new SacPostal();
		sac1.ajoute(lettre1);
		sac1.ajoute(lettre2);
		sac1.ajoute(colis1);
	}
	
	@Test
	public void testValeurRemboursement() {
		assert(Math.abs(sac1.valeurRemboursement()-116.5f)<tolerancePrix);
	}
	
	@Test
	public void testTarifAffranchissement() {
		int supplement = 0;
		if(colis1.getEmballagePoste())
			supplement=(int)ColisExpress.getTarifEmballage();
		assertEquals((int)colis1.tarifAffranchissement(),(int)ColisExpress.getAffranchissementColisExpress()+supplement);
	}
	
	@Test
	public void testVolume() {
		assert(sac1.getVolume()-0.025359999558422715f<toleranceVolume);
		SacPostal sac2 = sac1.extraireV1("7877");
		assert(sac2.getVolume()-0.02517999955569394f<toleranceVolume);
	}
	
	@Test
	public void testColisExpressTropLourd() {
		float poid=40f;
		try {
			ColisExpress colisTest = new ColisExpress("Sofia", "Montpellier", "34000", poid, 200f, Recommandation.zero, "Contient des fruits", 45.99f,true);
			assertTrue(colisTest.getEmballagePoste());
			colisTest.affiche();
		} catch(Exception e) {
			assertTrue(e.getMessage().equals("poids incohérent, votre colis ne pourra pas être acheminé."));
		}
	}
	
	@Test
	public void testTypeColisExpress() {
		assertEquals(colis1.typeObjetPostal(),"Colis express");
	}

}
