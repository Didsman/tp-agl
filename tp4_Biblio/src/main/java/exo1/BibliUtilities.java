package exo1;

import java.time.Clock;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class BibliUtilities {
	GlobalBibliographyAccess gbAccess;
	public static int delaiInventaire=12;
	private Clock clock;
	
	public BibliUtilities() {
		super();
		gbAccess = new GlobalBibliographyAccess();
		clock = Clock.systemDefaultZone();
	}
	
	public NoticeStatus ajoutNotice(String isbn) throws AjoutImpossibleException {
		NoticeStatus res=null;
		try {
			NoticeBibliographique nb=gbAccess.getNoticeFromIsbn(isbn);
			res = Bibliothèque.getInstance().addNotice(nb);
		} catch (IncorrectIsbnException e) {
			throw new AjoutImpossibleException();
		}
		return res;
	}
	
	public List<NoticeBibliographique> chercherNoticesConnexes(NoticeBibliographique ref){
		ArrayList<NoticeBibliographique> res = new ArrayList<NoticeBibliographique>();
		res = gbAccess.noticesDuMemeAuteurQue(ref);
		Set<String> nameSet = new HashSet<>();
		
		return res.stream()
				.filter(n-> !n.getTitre().equals(ref.getTitre()))
				.filter(e-> nameSet.add(e.getTitre()))
				.limit(5)
				.collect(Collectors.toList());
	}
	
	public boolean prevoirInventaire() {	
		return Math.abs(LocalDate.now(clock).until(Bibliothèque.getInstance().getLastInventaire()).toTotalMonths()) >= delaiInventaire;
	}
}
