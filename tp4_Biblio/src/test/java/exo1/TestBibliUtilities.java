package exo1;

import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;
import java.sql.Blob;
import java.time.Clock;
import java.time.LocalDate;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class TestBibliUtilities {
	
	private NoticeBibliographique n1;
	private NoticeBibliographique n2;
	private NoticeBibliographique n3;
	private NoticeBibliographique n4;
	private NoticeBibliographique n5;
	private NoticeBibliographique n6;
	private NoticeBibliographique n7;
	private NoticeBibliographique n8;
	
	private Bibliothèque bibli;
	
	private ArrayList<NoticeBibliographique> listeNotices;
	
	@Mock
	Clock mockedClock;
	
	@Mock
	IGlobalBibliographyAccess glob;
	
	@InjectMocks
	BibliUtilities b = new BibliUtilities();
	
	@BeforeAll
	public void setUp(){
		n1 = new NoticeBibliographique("9782765409120", "Manuel de bibliographie générale", "Marie-Hélène Prévoteau");
		n2 = new NoticeBibliographique("9780439136358", "Harry Potter and the Prisoner of Azkaban", "J.K. Rowling");
		n3 = new NoticeBibliographique("9791032917169", "Le Temps des Tempêtes", "Nicolas Sarkozy");
		n4 = new NoticeBibliographique("9780192829726", "Les Fleurs du mal", "Charles Baudelaire");
		n5 = new NoticeBibliographique("9782290227091", "Passions", "Nicolas Sarkozy");
		n6 = new NoticeBibliographique("9780439785969", "Harry Potter and the half blood prince", "J.K. Rowling");
		n7 = new NoticeBibliographique("9781338099133", "Harry Potter and the cursed child part1&2", "J.K. Rowling");
		n8 = new NoticeBibliographique("9782733504376", "Promenades", "Nicolas Sarkozy");
	
		listeNotices = new ArrayList<NoticeBibliographique>();
		bibli = Bibliothèque.getInstance();
	}

	@AfterAll
	public void tearDown(){
	}

	@Test
	public void testAjoutNoticeCorrectes() throws IncorrectIsbnException,AjoutImpossibleException{
		bibli.addNotice(n1);
		bibli.addNotice(n2);
		bibli.addNotice(n3);
	}
	
	@Test
	public void testAjoutNoticeIncorrectes() throws IncorrectIsbnException,AjoutImpossibleException{
		when(glob.getNoticeFromIsbn("Ne Fonctionne Pas")).thenThrow(IncorrectIsbnException.class);
		assertThrows(AjoutImpossibleException.class, ()->{
			b.ajoutNotice("Ne Fonctionne Pas");
		});
	}

}
